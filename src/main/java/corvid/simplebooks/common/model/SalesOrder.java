package corvid.simplebooks.common.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.io.Serializable;

/**
 * Created by kodero on 1/2/17.
 */
@Entity
@DiscriminatorValue("SALES_ORDER")
public class SalesOrder extends Transaction implements Serializable {
}
