package corvid.simplebooks.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by kodero on 2/20/16.
 */
@Entity
@Table(name = "transaction_items")
public class TransactionItem implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "description")
    private String description;

    @JoinColumn(name = "item", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Item item;

    @Column(name = "item_name")
    private String itemName;

    @Column(name = "item_code")
    private String itemCode;

    @Column(name = "quantity")
    private BigDecimal quantity;

    @Column(name = "qty_ordered")
    private BigDecimal qtyOrdered;

    @Column(name = "rate")
    private BigDecimal rate;

    @Column(name = "rate_incl")
    private BigDecimal rateIncl;

    @Column(name = "actual_discount")
    private BigDecimal actualDiscount;

    @Column(name = "actual_tax")
    private BigDecimal actualTax;

    @Column(name = "line_subtotal")
    private BigDecimal lineSubTotal;//total without tax

    @Column(name = "line_total")
    private BigDecimal lineTotal;

    @JoinColumn(name = "transaction", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.LAZY)
    private Transaction transaction;

    public BigDecimal getLineTotal() {
        return lineTotal;
    }

    public void setLineTotal(BigDecimal lineTotal) {
        this.lineTotal = lineTotal;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public BigDecimal getLineSubTotal() {
        return lineSubTotal;
    }

    public void setLineSubTotal(BigDecimal lineSubTotal) {
        this.lineSubTotal = lineSubTotal;
    }

    public BigDecimal getActualTax() {
        return actualTax;
    }

    public void setActualTax(BigDecimal actualTax) {
        this.actualTax = actualTax;
    }

    public BigDecimal getActualDiscount() {
        return actualDiscount;
    }

    public void setActualDiscount(BigDecimal actualDiscount) {
        this.actualDiscount = actualDiscount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getRateIncl() {
        return rateIncl;
    }

    public void setRateIncl(BigDecimal rateIncl) {
        this.rateIncl = rateIncl;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public BigDecimal getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(BigDecimal qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }
}
