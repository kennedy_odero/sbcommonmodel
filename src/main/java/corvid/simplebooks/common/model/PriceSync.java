package corvid.simplebooks.common.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by kodero on 1/30/17.
 */
@Entity
@Table(name = "price_syncs")
public class PriceSync implements Serializable{

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(name = "stock_link")
    private Integer stockLink;

    @Column(name = "item_code")
    private String itemCode;

    @Column(name = "description")
    private String description;

    @Column(name = "cost")
    private BigDecimal cost;

    @Column(name = "markup_rate")
    private BigDecimal markupRate;

    @Column(name = "excl_price")
    private BigDecimal exclPrice;

    @Column(name = "incl_price")
    private BigDecimal inclPrice;

    @Column(name = "new_item_marker")
    private String newItemMarker;

    @Column(name = "processed")
    private Boolean processed;

    public PriceSync(){

    }

    public PriceSync(Integer stockLink, String itemCode, String description, BigDecimal cost, BigDecimal markupRate, BigDecimal exclPrice, BigDecimal inclPrice, String newItemMarker){
        setStockLink(stockLink);
        setItemCode(itemCode);
        setDescription(description);
        setCost(cost);
        setMarkupRate(markupRate);
        setExclPrice(exclPrice);
        setInclPrice(inclPrice);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStockLink() {
        return stockLink;
    }

    public void setStockLink(Integer stockLink) {
        this.stockLink = stockLink;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public BigDecimal getMarkupRate() {
        return markupRate;
    }

    public void setMarkupRate(BigDecimal markupRate) {
        this.markupRate = markupRate;
    }

    public BigDecimal getExclPrice() {
        return exclPrice;
    }

    public void setExclPrice(BigDecimal exclPrice) {
        this.exclPrice = exclPrice;
    }

    public BigDecimal getInclPrice() {
        return inclPrice;
    }

    public void setInclPrice(BigDecimal inclPrice) {
        this.inclPrice = inclPrice;
    }

    public String getNewItemMarker() {
        return newItemMarker;
    }

    public void setNewItemMarker(String newItemMarker) {
        this.newItemMarker = newItemMarker;
    }

    public Boolean isProcessed() {
        return processed;
    }

    public void setProcessed(Boolean processed) {
        this.processed = processed;
    }
}
